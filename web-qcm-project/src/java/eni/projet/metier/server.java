/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.metier;

import eni.projet.entity.Personne;
import java.util.ArrayList;

/**
 *
 * @author ETY0004
 */
public interface server {
    public ArrayList<Personne> getPersonnes();
    public void setPersonnes(ArrayList<Personne> personnes);
    public void setPersonne(Personne personne);
    public void unsetPersonne(Personne personne);        
    public void unsetPersonnes(ArrayList<Personne> personnes);        
    public int getCountUsers();
            
}

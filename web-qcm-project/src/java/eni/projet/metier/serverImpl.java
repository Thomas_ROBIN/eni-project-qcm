/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.metier;

import eni.projet.entity.Personne;
import java.util.ArrayList;

/**
 *
 * @author ETY0004
 */
public class serverImpl implements server{
    	private static server instance;
	private int countUser = 0;
        private ArrayList<Personne> personnes;

	private serverImpl() {
	}
	
	public static server getInstance(){
		if(instance == null){
			instance = new serverImpl();
		}
		return instance;
	}
    
            @Override
    public int getCountUsers() {
        return this.getPersonnes().size();
    }

            @Override
    public ArrayList<Personne> getPersonnes() {
        return personnes;
    }

            @Override
    public void setPersonnes(ArrayList<Personne> personnes) {
        this.personnes = personnes;
    }
    
            @Override
    public void setPersonne(Personne personne) {
        this.personnes.add(personne);
    }

    @Override
    public void unsetPersonne(Personne personne) {
        this.personnes.remove(personne);
    }

    @Override
    public void unsetPersonnes(ArrayList<Personne> personnes) {
        this.personnes.clear();
    }
}

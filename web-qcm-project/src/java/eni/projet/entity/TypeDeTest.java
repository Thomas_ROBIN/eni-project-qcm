/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.entity;
import java.util.ArrayList;

/**
 *
 * @author ETY0004
 */
public class TypeDeTest {
    int idTypeDeTest;
    ArrayList<Test> tests = new ArrayList<Test>();
    String libellerTypeDeTexte;

    public TypeDeTest(String libellerTypeDeTexte) {
        this.libellerTypeDeTexte = libellerTypeDeTexte;
    }
    
    public TypeDeTest(String libellerTypeDeTexte, ArrayList<Test> tests) {
        this.libellerTypeDeTexte = libellerTypeDeTexte;
        setTests(tests);
    }

    public TypeDeTest() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public int getIdTypeDeTest() {
        return idTypeDeTest;
    }

    public ArrayList<Test> getTests() {
        return tests;
    }

    public void setTests(ArrayList<Test> tests) {
        this.tests = tests;
    }

    public String getLibellerTypeDeTexte() {
        return libellerTypeDeTexte;
    }

    public void setLibellerTypeDeTexte(String libellerTypeDeTexte) {
        this.libellerTypeDeTexte = libellerTypeDeTexte;
    }

    public int idTest() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

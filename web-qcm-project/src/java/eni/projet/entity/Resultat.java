/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.entity;

/**
 *
 * @author ETY0006
 */
public class Resultat{
    private int idResultat;
    private int idTest;

    public Resultat(int idResultat, int idTest) {
        this.idResultat = idResultat;
        this.idTest = idTest;
    }

    public Resultat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     
    public int getIdResultat() {
        return idResultat;
    }

    public void setIdResultat(int idResultat) {
        this.idResultat = idResultat;
    }

    public int getIdTest() {
        return idTest;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.entity;

import java.util.Date;

/**
 *
 * @author ETY0006
 */
public class ModuleEnCours{
    private int idModuleEncours;
    private int idTest;
    private Date  dateDebut;
    private Date dateFin;

      public ModuleEnCours() {
          
    }
    
    public ModuleEnCours(int idModuleEncours, int idTest, Date dateDebut, Date dateFin) {
        this.idModuleEncours = idModuleEncours;
        this.idTest = idTest;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }

    public int getIdModuleEncours() {
        return idModuleEncours;
    }

    public void setIdModuleEncours(int idModuleEncours) {
        this.idModuleEncours = idModuleEncours;
    }

    public int getIdTest() {
        return idTest;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

  
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.entity;

/**
 *
 * @author ETY0006
 */
public class Question {
    private int idQuestion;
    private String typeQuestion;
    private String libelleQuestion;

    public Question() {
    }

    public Question(int idQuestion, String typeQuestion, String libelleQuestion) {
        this.idQuestion = idQuestion;
        this.typeQuestion = typeQuestion;
        this.libelleQuestion = libelleQuestion;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getTypeQuestion() {
        return typeQuestion;
    }

    public void setTypeQuestion(String typeQuestion) {
        this.typeQuestion = typeQuestion;
    }

    public String getLibelleQuestion() {
        return libelleQuestion;
    }

    public void setLibelleQuestion(String libelleQuestion) {
        this.libelleQuestion = libelleQuestion;
    }

   
    
    
}

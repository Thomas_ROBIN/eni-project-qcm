/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.entity;

import java.util.ArrayList;
import java.util.Date;


/**
 *
 * @author ETY0006
 */
public class Formateur extends Personne{

    public Formateur(String Nom, String prenom, String Mail, String login, String mdp, Date dateNaissance) {
        super(Nom, prenom, Mail, login, mdp, dateNaissance);
    }
    private int idPersonne;
    private int idFormateur;

    ArrayList<Formation> formation = new ArrayList<Formation>();

        public Formateur() {
    }
        
    public Formateur(int idPersonne, int idFormateur) {
        this.idPersonne = idPersonne;
        this.idFormateur = idFormateur;
    }

    public int getIdFormateur() {
        return idFormateur;
    }

    public void setIdFormateur(int idFormateur) {
        this.idFormateur = idFormateur;
    }

    public ArrayList<Formation> getFormation() {
        return formation;
    }

    public void setFormation(ArrayList<Formation> formation) {
        this.formation = formation;
    }
   
    @Override
    public String toString() {
        return "Personne{" + ", Nom=" + this.getNom() + 
                ", prenom=" + this.getPrenom() + 
                ", Mail=" + this.getMail() + 
                ", login=" + this.getLogin() + 
                ", mdp=" + this.getMdp() + 
                ", dateNaissance=" + this.getDateNaissance() + '}';
    }

    public int getIdFormation() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

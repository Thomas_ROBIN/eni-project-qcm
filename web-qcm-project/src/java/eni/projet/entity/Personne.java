/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.entity;

import java.util.Date;

/**
 * @author ETY0006
0 */
public class Personne {
    int idPersonne;
    private String Nom;
    private String prenom;
    private String Mail;
    private String login;
    private String mdp;
    private Date dateNaissance;
    private String sexe;

    public Personne() {
    }
    
    public Personne(String Nom, String prenom, String Mail, String login, String mdp, Date dateNaissance) {
        this.Nom = Nom;
        this.prenom = prenom;
        this.Mail = Mail;
        this.login = login;
        this.mdp = mdp;
        this.dateNaissance = dateNaissance;
        this.sexe = sexe;

    }

    public int getIdPersonne() {
        return idPersonne;
    }
    
    public String getNom() {
        return Nom;
    }
    
    public String getPrenom() {
        return prenom;
    }
    
    public String getMail() {
        return Mail;
    }
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }
    
    public Date getDateNaissance() {
        return dateNaissance;
    }
    
    public String getSexe() {
        return sexe;
    }
        
        @Override
    public String toString() {
        return "Personne{" + ", Nom=" + Nom + ", prenom=" + prenom + ", Mail=" + Mail + ", login=" + login + ", mdp=" + mdp + ", dateNaissance=" + dateNaissance + '}';
    }
    
}

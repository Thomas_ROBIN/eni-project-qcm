/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.entity;

/**
 *
 * @author ETY0006
 */
public class Reponse {
    
    private int idQuestion;
    private int idResponse;
    private String libelleReponse;
    private String reponseImage;
    private int reponseOrdre;
    private boolean reponseBonne;

    public Reponse() {
    }

    public Reponse(int idQuestion, int idResponse, String libelleReponse, String reponseImage, int reponseOrdre, boolean reponseBonne) {
        this.idQuestion = idQuestion;
        this.idResponse = idResponse;
        this.libelleReponse = libelleReponse;
        this.reponseImage = reponseImage;
        this.reponseOrdre = reponseOrdre;
        this.reponseBonne = reponseBonne;
    }

    public Reponse(int idQuestion, int idResponse, String libelleReponse, int reponseOrdre, boolean reponseBonne) {
        this.idQuestion = idQuestion;
        this.idResponse = idResponse;
        this.libelleReponse = libelleReponse;
        this.reponseOrdre = reponseOrdre;
        this.reponseBonne = reponseBonne;
    }

    public Reponse(int idQuestion, int idResponse, String libelleReponse, String reponseImage, boolean reponseBonne) {
        this.idQuestion = idQuestion;
        this.idResponse = idResponse;
        this.libelleReponse = libelleReponse;
        this.reponseImage = reponseImage;
        this.reponseBonne = reponseBonne;
    }

    public Reponse(int idQuestion, int idResponse, String libelleReponse, boolean reponseBonne) {
        this.idQuestion = idQuestion;
        this.idResponse = idResponse;
        this.libelleReponse = libelleReponse;
        this.reponseBonne = reponseBonne;
    }

    public int getIdResponse() {
        return idResponse;
    }
    
    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getLibelleReponse() {
        return libelleReponse;
    }

    public void setLibelleReponse(String libelleReponse) {
        this.libelleReponse = libelleReponse;
    }

    public String getReponseImage() {
        return reponseImage;
    }

    public void setReponseImage(String reponseImage) {
        this.reponseImage = reponseImage;
    }

    public int getReponseOrdre() {
        return reponseOrdre;
    }

    public void setReponseOrdre(int reponseOrdre) {
        this.reponseOrdre = reponseOrdre;
    }

    public boolean isReponseBonne() {
        return reponseBonne;
    }

    public void setReponseBonne(boolean reponseBonne) {
        this.reponseBonne = reponseBonne;
    }
    
    
    
}

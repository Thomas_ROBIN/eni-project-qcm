/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.entity;

/**
 *
 * @author ETY0006
 */
public class Formation {
    private int idFormation;
    private int idFormateur;
    private String nomFormation;
    private int coefficient;
    private Formateur formateur;

   public Formation() { 
    }

    public Formation(int idFormation, int idFormateur, String nomFormation, int coefficient) {
        this.idFormation = idFormation;
        this.idFormateur = idFormateur;
        this.nomFormation = nomFormation;
        this.coefficient = coefficient;
    }

    public int getIdFormation() {
        return idFormation;
    }

    public void setIdFormation(int idFormation) {
        this.idFormation = idFormation;
    }

    public int getIdFormateur() {
        return idFormateur;
    }

    public void setIdFormateur(int idFormateur) {
        this.idFormateur = idFormateur;
    }

    public String getNomFormation() {
        return nomFormation;
    }

    public void setNomFormation(String nomFormation) {
        this.nomFormation = nomFormation;
    }

    public int getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(int coefficient) {
        this.coefficient = coefficient;
    }
    
        public Formateur getFormateur() {
        return formateur;
    }

    public void setFormateur(Formateur formateur) {
        this.formateur = formateur;
    }
}

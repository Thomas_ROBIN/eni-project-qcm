/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.entity;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author ETY0006
 */
public class Candidat extends Personne {
    int idCandidat;
    ArrayList<Test> tests = new ArrayList<Test>();
    ArrayList<Resultat> resultats = new ArrayList<Resultat>();

    public Candidat() {
    }

    public Candidat(int idPersonne, String Nom, String prenom, String Adresse, String Mail, String login, String mdp, Date dateNaissance, ArrayList test, ArrayList resultat) {
        super(Nom, prenom, Mail, login, mdp, dateNaissance);
        setTests(test);
        setResultats(resultat);
    }
    
    public Candidat(int idPersonne, String Nom, String prenom, String Adresse, String Mail, String login, String mdp, Date dateNaissance, Test test, Resultat resultat) {
        super( Nom, prenom, Mail, login, mdp, dateNaissance);
        setTest(test);
        setResultat(resultat);
    }
    
    public Candidat(int idPersonne, String Nom, String prenom, String Adresse, String Mail, String login, String mdp, Date dateNaissance) {
        super(Nom, prenom, Mail, login, mdp, dateNaissance);
    }
    
    public ArrayList<Test> getTests() {
        return tests;
    }

    public ArrayList<Resultat> getResultats() {
        return resultats;
    }

    public void setTests(ArrayList<Test> tests) {
        this.tests = tests;
    }

    public void setResultats(ArrayList<Resultat> resultats) {
        this.resultats = resultats;
    }
    
    public void setTest(Test test) {
        this.tests.add(test);
    }

    public void setResultat(Resultat resultat) {
        this.resultats.add(resultat);
    }
}

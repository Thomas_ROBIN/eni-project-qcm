/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.entity;

import java.util.ArrayList;

/**
 *
 * @author ETY0004
 */
public class Test {
    int idTest;

    TypeDeTest typeDeTest;
    Candidat candidat;
    ArrayList<Question> questions = new ArrayList<Question>();
    ModuleEnCours moduleEnCours;

    public Test(TypeDeTest typeDeTest, Candidat candidat, ArrayList questions) {
        this.typeDeTest = typeDeTest;
        this.candidat = candidat;
        this.questions = questions;
    }

    public Test() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public TypeDeTest getTypeDeTest() {
        return typeDeTest;
    }

    public void setTypeDeTest(TypeDeTest typeDeTest) {
        this.typeDeTest = typeDeTest;
    }

    public Candidat getCandidat() {
        return candidat;
    }

    public void setCandidat(Candidat candidat) {
        this.candidat = candidat;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }
    
    public int getIdTest() {
        return idTest;
    }

    public ModuleEnCours getModuleEnCour() {
        return moduleEnCours;
    }
    
    public Question getQuestion(int idQuestion) {
        Question result = new Question(); 
        for(Question question : getQuestions()) { 
           if(question.getIdQuestion() == idQuestion) {
               result = question;
           }
        }
        return result;
    }

    public void setQuestions(Question question) {
        this.questions.add(question);
    }

    public int getIdCandidat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIdQuestion() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIdModuleEnCours() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

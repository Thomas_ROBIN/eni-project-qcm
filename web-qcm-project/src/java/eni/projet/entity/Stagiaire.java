/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.entity;

import java.util.ArrayList;

/**
 *
 * @author ETY0004
 */
public class Stagiaire extends Personne{
    int idPersonne;
    int idStagiaire;
    
    ArrayList<Formation> formation = new ArrayList<Formation>();

    public Stagiaire(int idPersonne, int idStagiaire) {
        this.idPersonne = idPersonne;
        this.idStagiaire = idStagiaire;
    }

    public Stagiaire() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIdPersonne() {
        return idPersonne;
    }

    public void setIdPersonne(int idPersonne) {
        this.idPersonne = idPersonne;
    }

    public int getIdStagiaire() {
        return idStagiaire;
    }

    public void setIdStagiaire(int idStagiaire) {
        this.idStagiaire = idStagiaire;
    }

    public ArrayList<Formation> getFormation() {
        return formation;
    }

    public void setFormation(ArrayList<Formation> formation) {
        this.formation = formation;
    }

    public int getIdFormation() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}

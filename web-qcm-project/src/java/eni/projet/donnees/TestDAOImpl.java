/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.donnees;

import eni.projet.entity.Test;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ETY0006
 */
public class TestDAOImpl implements TestDAO {

    private static TestDAO instance;

    private TestDAOImpl() {

    }

    public static TestDAO getInstance() {
        if (instance == null) {
            instance = new TestDAOImpl();
        }
        return instance;
    }

    @Override
    public List<Test> selectAll() throws TestDataException {
        List<Test> result = new ArrayList<Test>();
        Connection con = null;
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new TestDataException("problème de Driver");
        }

        String sql = "SELECT idCandidat, idQuestion, idModuleEnCours FROM Test";

        PreparedStatement stmt;
        try {
            stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Test test = new Test();

                result.add(test);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new TestDataException("problème de select");
        }

        return result;
    }

    @Override
    public void insertTest(Test test) throws TestDataException {
        Connection con = null;
        // recupere la connection Ã  la base
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new TestDataException("problème de Driver");
        }

        // dÃ©fini la requete parametree
        String sql = "INSERT INTO Test (idCandidat, idQuestion, idModuleEnCours) VALUES (?, ?, ?)";

        // execute la requete
        PreparedStatement stmt;
        try {
            // Creation du statement et insertion des paramatres
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, test.getIdCandidat());
            stmt.setInt(2, test.getIdQuestion());
            stmt.setInt(3, test.getIdModuleEnCours());
            stmt.executeUpdate();

            con.close();
        } catch (SQLException e) {
            throw new TestDataException("probleme insertion");
        }
    }

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.donnees;

import eni.projet.entity.Candidat;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ETY0006
 */
public class CandidatDAOImpl implements CandidatDAO {

    private static CandidatDAO instance;

    private CandidatDAOImpl() {

    }

    public static CandidatDAO getInstance() {
        if (instance == null) {
            instance = new CandidatDAOImpl();
        }
        return instance;
    }

    @Override
    public List<Candidat> selectAll() throws CandidatDataException {
        List<Candidat> result = new ArrayList<Candidat>();
        Connection con = null;
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new CandidatDataException("problème de Driver");
        }

        String sql = "SELECT idPersonne FROM Candidat";

        PreparedStatement stmt;
        try {
            stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Candidat cand = new Candidat();

                result.add(cand);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CandidatDataException("problème de select");
        }

        return result;
    }

    @Override
    public void insertCandidat(Candidat candidat) throws CandidatDataException {
        Connection con = null;
        // recupere la connection Ã  la base
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new CandidatDataException("problème de Driver");
        }

        // dÃ©fini la requete parametree
        String sql = "INSERT INTO Candidat (idPersonne) VALUES (?)";

        // execute la requete
        PreparedStatement stmt;
        try {
            // Creation du statement et insertion des paramatres
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, candidat.getIdPersonne());

            stmt.executeUpdate();

            con.close();
        } catch (SQLException e) {
            throw new CandidatDataException("probleme insertion");
        }
    }

}
package eni.projet.donnees;

import java.util.List;

import eni.projet.entity.Personne;

/**
 *
 * @author ETY0006
 */
public interface PersonneDAO {

    public void insertPersonne(Personne personne) throws PersonneDataException;

    public List<Personne> selectAll() throws PersonneDataException;
}

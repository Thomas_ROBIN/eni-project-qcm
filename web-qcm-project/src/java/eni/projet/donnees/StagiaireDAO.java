package eni.projet.donnees;

import java.util.List;

import eni.projet.entity.Stagiaire;

/**
 *
 * @author ETY0006
 */
public interface StagiaireDAO {

    public void insertStagiaire(Stagiaire stagiaire) throws StagiaireDataException;

    public List<Stagiaire> selectAll() throws StagiaireDataException;
}

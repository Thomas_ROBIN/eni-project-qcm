package eni.projet.donnees;

import java.util.List;

import eni.projet.entity.Resultat;

/**
 *
 * @author ETY0006
 */
public interface ResultatDAO {

    public void insertResultat(Resultat resultat) throws ResultatDataException;

    public List<Resultat> selectAll() throws ResultatDataException;
}

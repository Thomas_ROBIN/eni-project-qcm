/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.donnees;

import eni.projet.entity.Resultat;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ETY0006
 */
public class ResultatDAOImpl implements ResultatDAO {

    private static ResultatDAO instance;

    private ResultatDAOImpl() {

    }

    public static ResultatDAO getInstance() {
        if (instance == null) {
            instance = new ResultatDAOImpl();
        }
        return instance;
    }

    @Override
    public List<Resultat> selectAll() throws ResultatDataException {
        List<Resultat> result = new ArrayList<Resultat>();
        Connection con = null;
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new ResultatDataException("problème de Driver");
        }

        String sql = "SELECT idTest FROM Resultat";

        PreparedStatement stmt;
        try {
            stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Resultat resu = new Resultat();

                result.add(resu);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ResultatDataException("problème de select");
        }

        return result;
    }

    @Override
    public void insertResultat(Resultat resultat) throws ResultatDataException {
        Connection con = null;
        // recupere la connection Ã  la base
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new ResultatDataException("problème de Driver");
        }

        // dÃ©fini la requete parametree
        String sql = "INSERT INTO Resultat (idTest) VALUES (?)";

        // execute la requete
        PreparedStatement stmt;
        try {
            // Creation du statement et insertion des paramatres
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, resultat.getIdTest());;
            stmt.executeUpdate();

            con.close();
        } catch (SQLException e) {
            throw new ResultatDataException("probleme insertion");
        }
    }

}
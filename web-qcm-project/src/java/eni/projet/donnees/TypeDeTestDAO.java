package eni.projet.donnees;

import java.util.List;

import eni.projet.entity.TypeDeTest;

/**
 *
 * @author ETY0006
 */
public interface TypeDeTestDAO {

    public void insertTypeDeTest(TypeDeTest typeDeTest) throws TypeDeTestDataException;

    public List<TypeDeTest> selectAll() throws TypeDeTestDataException;
}

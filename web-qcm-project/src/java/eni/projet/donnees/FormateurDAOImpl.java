/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.donnees;

import eni.projet.entity.Formateur;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ETY0006
 */
public class FormateurDAOImpl implements FormateurDAO {

    private static FormateurDAO instance;

    private FormateurDAOImpl() {

    }

    public static FormateurDAO getInstance() {
        if (instance == null) {
            instance = new FormateurDAOImpl();
        }
        return instance;
    }

    @Override
    public List<Formateur> selectAll() throws FormateurDataException {
        List<Formateur> result = new ArrayList<Formateur>();
        Connection con = null;
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new FormateurDataException("problème de Driver");
        }

        String sql = "SELECT idFormation, idPersonne FROM Formateur";

        PreparedStatement stmt;
        try {
            stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Formateur form = new Formateur();

                result.add(form);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new FormateurDataException("problème de select");
        }

        return result;
    }

    @Override
    public void insertFormateur(Formateur formateur) throws FormateurDataException {
        Connection con = null;
        // recupere la connection Ã  la base
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new FormateurDataException("problème de Driver");
        }

        // dÃ©fini la requete parametree
        String sql = "INSERT INTO Formateur (idFormation, idPersonne) VALUES (?,?)";

        // execute la requete
        PreparedStatement stmt;
        try {
            // Creation du statement et insertion des paramatres
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, formateur.getIdPersonne());
            stmt.setInt(2, formateur.getIdFormation());

            stmt.executeUpdate();

            con.close();
        } catch (SQLException e) {
            throw new FormateurDataException("probleme insertion");
        }
    }

}

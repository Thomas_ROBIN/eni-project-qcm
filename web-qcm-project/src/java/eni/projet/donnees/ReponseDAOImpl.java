/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.donnees;

import eni.projet.entity.Reponse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ETY0006
 */
public class ReponseDAOImpl implements ReponseDAO {

    private static ReponseDAO instance;

    private ReponseDAOImpl() {

    }

    public static ReponseDAO getInstance() {
        if (instance == null) {
            instance = new ReponseDAOImpl();
        }
        return instance;
    }

    @Override
    public List<Reponse> selectAll() throws ReponseDataException {
        List<Reponse> result = new ArrayList<Reponse>();
        Connection con = null;
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new ReponseDataException("problème de Driver");
        }

        String sql = "SELECT idCandidat, idQuestion, idModuleEnCours FROM Reponse";

        PreparedStatement stmt;
        try {
            stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Reponse repo = new Reponse();

                result.add(repo);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ReponseDataException("problème de select");
        }

        return result;
    }

    @Override
    public void insertReponse(Reponse reponse) throws ReponseDataException {
        Connection con = null;
        // recupere la connection Ã  la base
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new ReponseDataException("problème de Driver");
        }

        // dÃ©fini la requete parametree
        String sql = "INSERT INTO Reponse (idCandidat, idQuestion, idModuleEnCours) VALUES (?, ?, ?)";

        // execute la requete
        PreparedStatement stmt;
        try {
            // Creation du statement et insertion des paramatres
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, reponse.getIdQuestion());
            stmt.setString(2, reponse.getLibelleReponse());
            stmt.setString(3, reponse.getReponseImage());
            stmt.setInt(4, reponse.getReponseOrdre());
            stmt.setBoolean(5, reponse.isReponseBonne());
            stmt.executeUpdate();

            con.close();
        } catch (SQLException e) {
            throw new ReponseDataException("probleme insertion");
        }
    }

}
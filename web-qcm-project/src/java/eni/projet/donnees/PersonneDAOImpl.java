/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.donnees;

import eni.projet.entity.Personne;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ETY0006
 */
public class PersonneDAOImpl implements PersonneDAO {

    private static PersonneDAO instance;

    private PersonneDAOImpl() {

    }

    public static PersonneDAO getInstance() {
        if (instance == null) {
            instance = new PersonneDAOImpl();
        }
        return instance;
    }

    @Override
    public List<Personne> selectAll() throws PersonneDataException {
        List<Personne> result = new ArrayList<Personne>();
        Connection con = null;
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new PersonneDataException("problème de Driver");
        }

        String sql = "SELECT nom, prenom, mail, login, mdp, sexe FROM Personne";

        PreparedStatement stmt;
        try {
            stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Personne pers = new Personne();

                result.add(pers);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new PersonneDataException("problème de select");
        }

        return result;
    }

    @Override
    public void insertPersonne(Personne personne) throws PersonneDataException {
        Connection con = null;
        // recupere la connection Ã  la base
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new PersonneDataException("problème de Driver");
        }

        // dÃ©fini la requete parametree
        String sql = "INSERT INTO Personne (nom, prenom, mail, login, mdp, sexe) VALUES (?,?,?,?,?,?)";

        // execute la requete
        PreparedStatement stmt;
        try {
            // Creation du statement et insertion des paramatres
            stmt = con.prepareStatement(sql);
            stmt.setString(1, personne.getNom());
            stmt.setString(2, personne.getPrenom());
            stmt.setString(3, personne.getMail());
            stmt.setString(4, personne.getLogin());
            stmt.setString(5, personne.getMdp());
            stmt.setTimestamp(6, new java.sql.Timestamp(personne.getDateNaissance().getTime()));
            stmt.setString(7, personne.getSexe());

            stmt.executeUpdate();

            con.close();
        } catch (SQLException e) {
            throw new PersonneDataException("probleme insertion");
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.donnees;

import eni.projet.entity.TypeDeTest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ETY0006
 */
public class TypeDeTestDAOImpl implements TypeDeTestDAO {

    private static TypeDeTestDAO instance;

    private TypeDeTestDAOImpl() {

    }

    public static TypeDeTestDAO getInstance() {
        if (instance == null) {
            instance = new TypeDeTestDAOImpl();
        }
        return instance;
    }

    @Override
    public List<TypeDeTest> selectAll() throws TypeDeTestDataException {
        List<TypeDeTest> result = new ArrayList<TypeDeTest>();
        Connection con = null;
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new TypeDeTestDataException("problème de Driver");
        }

        String sql = "SELECT idTest, libellerTypeDeTest FROM TypeDeTest";

        PreparedStatement stmt;
        try {
            stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TypeDeTest type = new TypeDeTest();

                result.add(type);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new TypeDeTestDataException("problème de select");
        }

        return result;
    }

    @Override
    public void insertTypeDeTest(TypeDeTest typeDeTest) throws TypeDeTestDataException {
        Connection con = null;
        // recupere la connection Ã  la base
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new TypeDeTestDataException("problème de Driver");
        }

        // dÃ©fini la requete parametree
        String sql = "INSERT INTO TypeDeTest (idTest, libellerTypeDeTest) VALUES (?,?)";

        // execute la requete
        PreparedStatement stmt;
        try {
            // Creation du statement et insertion des paramatres
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, typeDeTest.idTest());
            stmt.setString(2, typeDeTest.getLibellerTypeDeTexte());

            stmt.executeUpdate();

            con.close();
        } catch (SQLException e) {
            throw new TypeDeTestDataException("probleme insertion");
        }
    }

}

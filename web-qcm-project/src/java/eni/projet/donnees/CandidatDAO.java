package eni.projet.donnees;

import java.util.List;

import eni.projet.entity.Candidat;

/**
 *
 * @author ETY0006
 */
public interface CandidatDAO {

    public void insertCandidat(Candidat candidat) throws CandidatDataException;

    public List<Candidat> selectAll() throws CandidatDataException;
}

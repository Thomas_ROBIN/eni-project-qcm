package eni.projet.donnees;

import java.util.List;

import eni.projet.entity.Test;

/**
 *
 * @author ETY0006
 */
public interface TestDAO {

    public void insertTest(Test test) throws TestDataException;

    public List<Test> selectAll() throws TestDataException;
}

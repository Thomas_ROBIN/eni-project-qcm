/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.donnees;

import eni.projet.entity.Question;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ETY0006
 */
public class QuestionDAOImpl implements QuestionDAO {

    private static QuestionDAO instance;

    private QuestionDAOImpl() {

    }

    public static QuestionDAO getInstance() {
        if (instance == null) {
            instance = new QuestionDAOImpl();
        }
        return instance;
    }

    @Override
    public List<Question> selectAll() throws QuestionDataException {
        List<Question> result = new ArrayList<Question>();
        Connection con = null;
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new QuestionDataException("problème de Driver");
        }

        String sql = "SELECT typeQuestion, libelleQuestion FROM Question";

        PreparedStatement stmt;
        try {
            stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Question ques = new Question();

                result.add(ques);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new QuestionDataException("problème de select");
        }

        return result;
    }

    @Override
    public void insertQuestion(Question question) throws QuestionDataException {
        Connection con = null;
        // recupere la connection Ã  la base
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new QuestionDataException("problème de Driver");
        }

        // dÃ©fini la requete parametree
        String sql = "INSERT INTO Question (typeQuestion, libelleQuestion) VALUES (?,?)";

        // execute la requete
        PreparedStatement stmt;
        try {
            // Creation du statement et insertion des paramatres
            stmt = con.prepareStatement(sql);
            stmt.setString(1, question.getTypeQuestion());
            stmt.setString(2, question.getLibelleQuestion());

            stmt.executeUpdate();

            con.close();
        } catch (SQLException e) {
            throw new QuestionDataException("probleme insertion");
        }
    }

}

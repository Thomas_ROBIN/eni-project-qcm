package eni.projet.donnees;

import java.util.List;

import eni.projet.entity.Formateur;

/**
 *
 * @author ETY0006
 */
public interface FormateurDAO {

    public void insertFormateur(Formateur formateur) throws FormateurDataException;

    public List<Formateur> selectAll() throws FormateurDataException;
}

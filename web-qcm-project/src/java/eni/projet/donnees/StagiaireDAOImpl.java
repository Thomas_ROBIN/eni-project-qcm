/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eni.projet.donnees;

import eni.projet.entity.Stagiaire;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ETY0006
 */
public class StagiaireDAOImpl implements StagiaireDAO {

    private static StagiaireDAO instance;

    private StagiaireDAOImpl() {

    }

    public static StagiaireDAO getInstance() {
        if (instance == null) {
            instance = new StagiaireDAOImpl();
        }
        return instance;
    }

    @Override
    public List<Stagiaire> selectAll() throws StagiaireDataException {
        List<Stagiaire> result = new ArrayList<Stagiaire>();
        Connection con = null;
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new StagiaireDataException("problème de Driver");
        }

        String sql = "SELECT idFormation, idPersonne FROM Stagiaire";

        PreparedStatement stmt;
        try {
            stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Stagiaire stag = new Stagiaire();

                result.add(stag);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new StagiaireDataException("problème de select");
        }

        return result;
    }

    @Override
    public void insertStagiaire(Stagiaire stagiaire) throws StagiaireDataException {
        Connection con = null;
        // recupere la connection Ã  la base
        try {
            con = ConnectionBDD.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            throw new StagiaireDataException("problème de Driver");
        }

        // dÃ©fini la requete parametree
        String sql = "INSERT INTO Stagiaire (idFormation, idPersonne) VALUES (?,?)";

        // execute la requete
        PreparedStatement stmt;
        try {
            // Creation du statement et insertion des paramatres
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, stagiaire.getIdPersonne());
            stmt.setInt(2, stagiaire.getIdFormation());

            stmt.executeUpdate();

            con.close();
        } catch (SQLException e) {
            throw new StagiaireDataException("probleme insertion");
        }
    }

}

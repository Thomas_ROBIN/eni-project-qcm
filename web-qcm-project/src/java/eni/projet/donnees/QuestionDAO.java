package eni.projet.donnees;

import java.util.List;

import eni.projet.entity.Question;

/**
 *
 * @author ETY0006
 */
public interface QuestionDAO {

    public void insertQuestion(Question question) throws QuestionDataException;

    public List<Question> selectAll() throws QuestionDataException;
}

package eni.projet.donnees;

import java.util.List;

import eni.projet.entity.Reponse;

/**
 *
 * @author ETY0006
 */
public interface ReponseDAO {

    public void insertReponse(Reponse reponse) throws ReponseDataException;

    public List<Reponse> selectAll() throws ReponseDataException;
}

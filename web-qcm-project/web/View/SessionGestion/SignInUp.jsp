<%-- 
    Document   : SignInUp
    Created on : 29 juin 2017, 09:27:31
    Author     : ETY0004
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>QCM - Connexion</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        
        <div class="container">
                <form class="col-md-3" action="${pageContext.request.contextPath}/SignIn" method="post">

                    <div class="form-group">
                        <label for="login">Login :</label>
                        <input type="text" class="form-control" id="login" name="">
                    </div>

                    <div class="form-group">
                        <label for="pwd">Mot de Passe :</label>
                        <input type="password" class="form-control" id="pwd" name="">
                    </div>

                    <button type="submit" class="btn btn-info btn-lg">Se Connecter</button>
                    <br></br>
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Inscription</button>
                </form> 

          <!-- Modal -->
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">QCM - inscription</h4>
                </div>
                    <form class="col-md-6" action="${pageContext.request.contextPath}/SignUp" method="post">
                        <div class="form-group">
                            <label for="nom">Nom : </label>
                            <input type="text" class="form-control" id="nom"  name="nom">
                        </div>
                        <div class="form-group">
                            <label for="prenom">Prenom : </label>
                            <input type="text" class="form-control" id="prenom"  name="prenom">
                        </div>
                        <div class="form-group">
                            <label for="adresse">Adresse Complète : </label>
                            <input type="text" class="form-control" id="adresse"  name="adresse">
                        </div>
                        <div class="form-group">
                            <label for="login">Login : </label>
                            <input type="text" class="form-control" id="login"  name="login">
                        </div>
                        <div class="form-group">
                            <label for="mdp">Mot de Passe : </label>
                            <input type="password" class="form-control" id="mdp"  name="mdp">
                        </div>
                        <div class="form-group">
                            <label for="mail">Mail : </label>
                            <input type="text" class="form-control" id="mail"  name="mail">
                        </div>
                        <div class="form-group">
                            <label for="dateNaissance">Date de Naissance : </label>
                            <input placeholder = "jj/mm/aaaa" type="date" class="form-control" id="dateNaissance"  name="dateNaissance">
                        </div>
                        <div class="form-group">
                            
                            !!!!!  Sexe !!!!!!

                            <label for="sexe">Vous êtes : </label>
                            <div class="radio">
                                <label><input type="radio" name="radioSexe">Candidat</label>
                                <label><input type="radio" name="radioSexe">Stagiaire</label>
                                <label><input type="radio" name="radioSexe">Formateur</label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-default">S'inscrire</button>
                    </form>  
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>    
    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</html>

